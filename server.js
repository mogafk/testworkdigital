var webpack = require('webpack')
var webpackDevMiddleware = require('webpack-dev-middleware')
var webpackHotMiddleware = require('webpack-hot-middleware')
var config = require('./webpack.config')

var ORM = require('./server/ORM.js');
var orm = new ORM();

var express = require('express')
var app = new express()
// var app = new (require('express'))()
var port = 3000

var compiler = webpack(config)
app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }))
app.use(webpackHotMiddleware(compiler))

app.get("/", function(req, res) {
  res.sendFile(__dirname + '/index.html')
})


app.use('/static', express.static('public'));

app.get("/products", function(req, res) {
    // В реальных условиях тут нужна обработка
    // Сейчас кормим базу запросами полагаясь на пользователя
    console.log('req.query', req.query)
    let query = {};
    if(req.query.tags !== undefined){
        query['tags'] = {
            $in: req.query.tags
        }
    }
    if(req.query.colors !== undefined){
        query['color'] = {
            $in: req.query.colors
        }
    }
    if(req.query.size !== undefined){
        query['size'] = req.query.size;
    }
    if(req.query.price !== undefined){
        query['price'] = {
            $gt: req.query['price'][0],
            $lt: req.query['price'][1]
        }
    }
    
    console.log('query', query, 'skip', req.query['skip'], 'limit', req.query['limit'])

    orm.getProducts(query, req.query['skip'], req.query['limit'])
        .then((products) => {
            console.log('res.length getProducts', products.length);
            // console.log('res of /products/', products);
            res.json(products);
        })
})
app.get("/categories", function(req, res) {
    console.log('req.query', req.query)
    orm.getCategories()
        .then((categories) => {
            console.log('res of /categories/', categories);
            res.json(categories);
        })
        .catch((error) => {
            console.log('error');
        })
})

app.listen(port, function(error) {
  if (error) {
    console.error(error)
  } else {
    console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port)
  }
})
