const mongoose = require('mongoose');
var Products = require('./collections/products.js');
var Categories = require('./collections/categories.js');
var fs = require('fs');
var _ = require('lodash');

class ORM {
    constructor(){
        this.db_url = 'mongodb://localhost:27017/shop';

        mongoose.connect(this.db_url, {server: {auto_reconnect: true}});
        this.db = mongoose.connection;

        this.loadFixtures();
    }

    getProducts(query={}, skip=0, limit=9){
        console.log('getProducts', skip, limit);
        return Products.find(query).skip(skip).limit(limit);
    }
    getCategories(){
        return Categories.find({});
    }

    addProduct(product){
        return new Products(product)
                    .save()
                    .then(() => {
                        console.log('успешное заполнение данными для теста products');
                    })
    }
    addCateogory(category){
        return new Categories(category)
                    .save()
                    .then(() => {
                        console.log('успешное заполнение данными для теста category');
                    })
    }

    async loadFixtures(){
        if(await Products.find({}).count() == 0){
            this._loadFixturesProducts();
        }

        if(await Categories.find({}).count() == 0){
            this._loadFixturesCategories();
        }
    }


    _loadFixturesProducts(){
        for(let i=0; i<50; i++){
            this.addProduct({
                name: `products number №${i}`,
                tags: [
                    _.sample(['jackets', 'blazers', 'suits', 'trousers', 'jeans', 'shirts']),
                    _.sample(['jackets', 'blazers', 'suits', 'trousers', 'jeans', 'shirts']),
                ],
                price: _.random(100, 300),
                category: _.sample(['woman', 'man', 'kids']),
                size: _.sample(['xs', 's', 'm', 'l', 'xl']),
                color:  [
                    _.sample(['green', 'blue', 'brown', 'white', 'grey', 'black']),
                    _.sample(['green', 'blue', 'brown', 'white', 'grey', 'black']),
                ],
                image: '1.png',
            })
        }
    }

    _loadFixturesCategories(){
        var fixtures = JSON.parse(fs.readFileSync('server/fixtures/categories.fixture.json', 'utf8'));
        _.each(fixtures, (fixture)=> {
            this.addCateogory(fixture);
        })
    }


}

module.exports = ORM;