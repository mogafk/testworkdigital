const mongoose = require('mongoose');
const schema = require('../models/category.model.js')

module.exports = mongoose.model('categories', schema);
