const mongoose = require('mongoose');
const schema = require('../models/product.model.js')

module.exports = mongoose.model('products', schema);