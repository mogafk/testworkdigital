const mongoose = require('mongoose');

module.exports = new mongoose.Schema({
    name: {type: String, required: true},
    tags: {type: Array, required: true},
    price: {type: Number, required: true},
    category: {type: String, required: true},
    image: {type: String, required: true},
    size: {type: Array, required: false},
    color: {type: Array, required: false},
})

