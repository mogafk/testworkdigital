const mongoose = require('mongoose');

module.exports = new mongoose.Schema({
    title: {type: String, required: true},
    //категория, допустим Mens
    //должна содежрать в себе теги shirt\jeans\etc
    tags: {type: Array, required: true},
    //bags так же является категорией, но
    //можно ли ее фильтровать по xl, xs, l, m?
    //а сколько таких товаров потенциально возможн
    //это поле необходимо, чтобы разрешать лишь
    //необходимые фильтры
    availableFilters: {type: Array, required: true},
})

