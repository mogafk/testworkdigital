import React, { PropTypes, Component } from 'react'
import Collapse from 'react-collapse'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import AccordionItem from './AccordionItem'
import * as productActions from '../actions/ProductActions'

AccordionItem
class Accordion extends Component {
  constructor(props){
    super(props);

    const {slideDown} = this.props;
    this.state = {
        slideDown
    }

    this.filterByTag = this.filterByTag.bind(this);
    this.toggleSlide = this.toggleSlide.bind(this);
  }

  filterByTag(listItem){
    console.log('filterByTag listItem', listItem);

    let { query } = this.props.products,
        tags = query.tags || [];
    this.props.applyFilter({
        ...query,
        tags: [
            ...tags,
            listItem.toLowerCase()
        ]
    });
  }

  toggleSlide(){
    this.setState({
        slideDown: !this.state.slideDown
    })
  }

  render() {
    // console.log('this.props Accordion', this.props)
    let { query } = this.props.products;
    let { list } = this.props
        ,childrens = list.map((listItem) => {
            return <AccordionItem 
             query={query}
             category={this.props.header}
             queryValue={listItem}
             applyFilter={this.props.applyFilter} 
            >
                {listItem}
            </AccordionItem>
        });
    let classname = `filter_header filter_header--category ${!this.state.slideDown ? 'filter_header--collapsed': ''}`
    return (
    <div>
        <div
        className={classname}
        onClick={this.toggleSlide}>
            { this.props.header }
        </div>
        <Collapse isOpened={this.state.slideDown}>
            <ul className='filter_content filter_content--tags'>
                {childrens}
            </ul>
        </Collapse>
    </div>
    )
  }
}

Accordion.propTypes = {
  header: PropTypes.string.isRequired,
  list: PropTypes.array.isRequired,
  slideDown: PropTypes.bool
}

Accordion.defaultProps = {
    slideDown: false
}

function mapStateToProps(state) {
  return {
    products: state.products
  }
}

function mapDispatchToProps(dispatch) {
  return {
    productActions: bindActionCreators(productActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Accordion);
