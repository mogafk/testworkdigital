import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as productActions from '../actions/ProductActions'

class LoadMoreProducts extends Component {
  constructor(props){
    super(props)

    console.log('props in LoadMoreProducts', props, this.props)
    this.nextPage = this.nextPage.bind(this);    
  }
  nextPage(){
    let { skip, limit, query } = this.props.products;
    this.props.productActions.nextPage(query, skip, limit);
  }
  render() {
    return ( 
        <div 
         className='products_load-more'
         onClick={this.nextPage}
        >
            Load More
        </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    products: state.products
  }
}

function mapDispatchToProps(dispatch) {
  return {
    productActions: bindActionCreators(productActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoadMoreProducts)




// Page.propTypes = {
//   year: PropTypes.number.isRequired,
//   photos: PropTypes.array.isRequired,
//   setYear: PropTypes.func.isRequired
// }
