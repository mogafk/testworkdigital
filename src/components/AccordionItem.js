import React, { Component } from 'react'

export default class AccordionItem extends Component {
  constructor(props){
    super(props)

    this.state = {
        checked: false
    }

    this.filterBy = this.filterBy.bind(this);
  }


  filterBy(){
    let { checked } = this.state;
    let { query, queryValue } = this.props,
        tags = query.tags || [];
    if(!checked){
      this.props.applyFilter({
          ...query,
          tags: [
              ...tags,
              queryValue.toLowerCase()
          ]
      });

      this.setState({
        checked: true
      })
    }else{
      tags = tags.filter((item) => item !== queryValue.toLowerCase())
      let queryNew = {
          ...query,
          tags: [
              ...tags
          ]
      };

      console.log('queryNew', queryNew)
      this.props.applyFilter(queryNew);

      this.setState({
        checked: false
      })
    }
  }

  render() {
    let classname = `${this.props.className ? this.props.className : ''} ${this.state.checked ? 'tag--checked' : 'tag--unchecked'}`
    return (
      <li 
       onClick={this.filterBy}
       className={classname}
      >
        {this.props.children}
      </li>
    )
  }
}
