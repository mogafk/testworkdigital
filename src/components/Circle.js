import React, { Component } from 'react'

export default class Circle extends Component {
  constructor(props){
    super(props)

    this.state = {
        colors: ['green', 'brown', 'yellow'],
        checked: false
    }

    this.filterBy = this.filterBy.bind(this);
  }


  filterBy(){
    let { checked } = this.state;
    let { query, queryKey, queryValue } = this.props;
    let prevVals = query[queryKey] || [];
    if(!checked){
      console.log('filter by color');

      this.props.applyFilter({
          ...query,
          [queryKey]: [
              ...prevVals,
              queryValue.toLowerCase()
          ]
      });


      this.setState({
        checked: true
      })
    }else{
      prevVals = prevVals.filter((item) => item !== queryValue)
      let queryNew = {
          ...query,
          [queryKey]: [
              ...prevVals
          ]
      };

      console.log('queryNew', queryNew)
      this.props.applyFilter(queryNew);

      this.setState({
        checked: false
      })
    }
  }

  render() {
    let classname = `${this.props.className} ${this.state.checked ? 'circle--checked' : 'circle--unchecked'}`
    return (
      <span 
       onClick={this.filterBy}
       className={classname}
      >
        {this.props.children}
      </span>
    )
  }
}
