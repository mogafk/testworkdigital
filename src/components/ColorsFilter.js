import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Circle from './Circle.js'
import * as productActions from '../actions/ProductActions'

class ColorsFilter extends Component {
  constructor(props){
    super(props)

    this.state = {
        colors: ['blue','green', 'beige', 'brown', 'wetclay','black', 'yellow', 'grey']
    }
  }

  render() {
    let { query } = this.props.products;
    var circles = this.state.colors.map((color) => {
        let classname = `circle circle--${color}`
        return <Circle 
                query={query}
                queryKey='colors'
                queryValue={color}
                applyFilter={this.props.applyFilter}
                className={classname}>
                    &nbsp;
               </Circle>
    })
    return (
        <div className='filters_filter filter filter--color'>
            <div className='filter_header'>
                colors
            </div>
            <div className='filter_content filter_content--circles'>
                {circles}
            </div>
        </div>
    )
  }
}


function mapStateToProps(state) {
  return {
    products: state.products
  }
}

function mapDispatchToProps(dispatch) {
  return {
    productActions: bindActionCreators(productActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ColorsFilter);