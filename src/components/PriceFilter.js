import React, { Component } from 'react'
import ReactSlider from 'react-slider' 
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as productActions from '../actions/ProductActions'

class PriceFilter extends Component {
  constructor(props){
    super(props)
    this.state = {
        priceStart: 100,
        priceEnd: 768
    }

    this.handleSlider = this.handleSlider.bind(this);
    this.filterByCost = this.filterByCost.bind(this);
  }
  
  filterByCost(values){
    console.log('filterByCost', values);
    let { query } = this.props;
    console.log('query', query);
    this.props.applyFilter({
        ...query,
        price: values
    })
  }

  handleSlider(values){
    console.log('handleSlider, ', values);
    this.setState({
        priceStart: values[0],
        priceEnd: values[1]
    })
  }

  render() {
    return (
        <div className='filters_filter filter filter--price'>
            <div className='filter_header'>
                price range
            </div>
            <div className='filter_content filter_content--slider'>
                <ReactSlider 
                min={50}
                max={1000}
                defaultValue={[this.state.priceStart, this.state.priceEnd]}
                className='slider' 
                barClassName='slider_bar'
                onAfterChange={this.filterByCost}
                onChange={this.handleSlider}
                withBars>
                    <div className='slider_handle'>
                        <span className='slider_price-value slider_price-start'>
                            {this.state.priceStart}
                        </span>
                    </div>
                    <div className='slider_handle'>
                        <span className='slider_price-value slider_price-end'>
                            {this.state.priceEnd}
                        </span>
                    </div>
                </ReactSlider>
            </div>
        </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    products: state.products
  }
}

function mapDispatchToProps(dispatch) {
  return {
    productActions: bindActionCreators(productActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PriceFilter);
