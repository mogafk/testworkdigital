import React, { Component } from 'react'

import Accordion from '../components/Accordion'

export default class HumanTypeFilter extends Component {
  render() {
    let { categories, applyFilter } = this.props;
    let childs = categories.map((category) => {
      return <Accordion
              applyFilter={applyFilter} 
              list={category.tags} 
              header={category.title} 
             />
    })
    return (
    <div className='filters_filter filter filter--human'>
      {childs}
    </div>
    )
  }
}

// Page.propTypes = {
//   year: PropTypes.number.isRequired,
//   photos: PropTypes.array.isRequired,
//   setYear: PropTypes.func.isRequired
// }
