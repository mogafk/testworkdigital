import React, { Component } from 'react'

export default class ProductSlip extends Component {
  render() {
    let {price, name} = this.props.product;
    let tags = this.props.tags.map((tag)=>{
        return <div>{tag}</div>
    })
    console.log(tags);
    return (
        <div className='slip'>
        <div className='slip_indent'>
            <span className={Math.random() > 0.7 ? 'slip_new-label' : 'hide' }>
                new
            </span>
            <img src='http://localhost:3000/static/shirt.jpg' className='slip_img' alt='' />
            <div className='slip_header'>
                {name}
                <small>Black Grey Plants</small>
            </div>
            <div className='slip_cost'>
                ${price}
            </div>
        </div>

        </div>
    )
  }
}

// Page.propTypes = {
//   year: PropTypes.number.isRequired,
//   photos: PropTypes.array.isRequired,
//   setYear: PropTypes.func.isRequired
// }
