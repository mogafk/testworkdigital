import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Circle from './Circle.js'
import * as productActions from '../actions/ProductActions'

class SizeFilter extends Component {
  constructor(props){
    super(props)

    this.state = {
        sizes: ['XS', 'S', 'M', 'L', 'XL']
    }

  }

  render() {
    let { query } = this.props.products;
    var circles = this.state.sizes.map((size) => {
        return <Circle 
                query={query}
                queryKey='size'
                queryValue={size}
                applyFilter={this.props.applyFilter}
                className='circle'>
                    {size}
               </Circle>
    })
    return (
        <div className='filters_filter filter filter--size'>
            <div className='filter_header'>
                size
            </div>
            <div className='filter_content filter_content--circles'>
                {circles}
            </div>
        </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    products: state.products
  }
}

function mapDispatchToProps(dispatch) {
  return {
    productActions: bindActionCreators(productActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SizeFilter);