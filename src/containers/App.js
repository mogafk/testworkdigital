import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Filters from './Filters'
import Products from './Products'

import * as filterActions from '../actions/FilterActions'
import * as productActions from '../actions/ProductActions'

import '../styles/main.scss'

class App extends Component {
  render() {
    const { categories, products } = this.props

    const { fetchCategories } = this.props.filterActions
    const { fetchProducts, applyFilter } = this.props.productActions
    console.log('this.props(App)', this.props);
  
    return (
      <div className='main-container clearfix'>
        <Filters applyFilter={applyFilter} fetchCategories={fetchCategories} categories={categories.categories} />
        <Products products={products.products} fetchProducts={fetchProducts} />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    categories: state.categories,
    products: state.products
  }
}

function mapDispatchToProps(dispatch) {
  return {
    filterActions: bindActionCreators(filterActions, dispatch),
    productActions: bindActionCreators(productActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
