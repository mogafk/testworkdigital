import React, { Component } from 'react'

import ProductSlip from '../components/ProductSlip'
import LoadMoreProducts from '../components/LoadMoreProducts'

import '../styles/products.scss'

export default class Products extends Component {
  constructor(props){
    super(props);

    this.state = {
      page: 1,
      pageSize: 9
    }
  }

  componentDidMount() {
    this.props.fetchProducts();
  }

  render() {
    console.log('this.props in Products', this.props)
    let { products } = this.props,
      slips = products.map((product) =>{
        return <ProductSlip product={product} tags={product.tags} />
      })

    return <div className='products'>
      <div className='clearfix'>
        {slips}
      </div>
      
      <LoadMoreProducts />
    </div>
  }
}