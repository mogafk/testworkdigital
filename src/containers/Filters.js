import React, { Component } from 'react'
// import { bindActionCreators } from 'redux'
// import { connect } from 'react-redux'
// import * as FilterActions from '../actions/FilterActions'

import HumanTypeFilter from '../components/HumanTypeFilter'
import ColorFilter from '../components/ColorsFilter'
import SizeFilter from '../components/SizeFilter'
import PriceFilter from '../components/PriceFilter'

import '../styles/filter-base.scss'

export default class Filters extends Component {
  componentDidMount() {
    this.props.fetchCategories();
  }

  render() {
    const { categories } = this.props
    const { applyFilter } = this.props

    return <div className='filters'>
      <HumanTypeFilter 
       categories={categories} 
       applyFilter={applyFilter}
      />
      <ColorFilter applyFilter={applyFilter}/>
      <SizeFilter applyFilter={applyFilter}/>
      <PriceFilter applyFilter={applyFilter}/>
    </div>
  }
}

// function mapStateToProps(state) {
//   return {
//     //user: state.user,
//    // page: state.page
//     categories: state.categories
//   }
// }

// function mapDispatchToProps(dispatch) {
//   return {
//     filterActions: bindActionCreators(FilterActions, dispatch)
//   }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(Filters)