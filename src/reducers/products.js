import {
    GET_PRODUCTS_REQUEST,
    GET_PRODUCTS_SUCCESS,
    GET_PRODUCTS_FAIL,
    NEXT_PAGE_REQUEST,
    NEXT_PAGE_SUCCESS,
    NEXT_PAGE_FAIL,
    APPLY_FILTER_START,
    APPLY_FILTER_SUCCESS,
    APPLY_FILTER_FAIL
} from '../constants/Page'

const initialState = {
  products: [],
  skip: 0, limit: 9,
  query: {},
  fetched: false
}

export default function products(state = initialState, action) {
  switch (action.type) {
    case GET_PRODUCTS_REQUEST:
      return { ...state }

    case GET_PRODUCTS_SUCCESS:
      return { 
        ...state, 
        ...action.payload,
        fetched: true }

    case GET_PRODUCTS_FAIL:
      return { ...state }



    case NEXT_PAGE_REQUEST:
        return { 
            ...state,
            fetched: false
        }

    case NEXT_PAGE_SUCCESS:
        console.log(NEXT_PAGE_SUCCESS, state, action.payload)
        return { 
            ...state,
            products: [
                ...state.products,
                ...action.payload.products
            ],
            // query: action.payload.query,
            skip: action.payload.skip,
            fetched: true
         }

    case NEXT_PAGE_FAIL:
        return { ...state }



    case APPLY_FILTER_START:
        return { 
            ...state,
            fetched: false
        }

    case APPLY_FILTER_SUCCESS:
        console.log(APPLY_FILTER_SUCCESS, state, action.payload)
        return {
            ...state,
            query: {
                ...state.query,
                ...action.payload.query
            },
            products: action.payload.products,
            skip: 0,
            fetched: true
         }

    case APPLY_FILTER_FAIL:
        return { ...state }


    default:
      return state;
  }

}
