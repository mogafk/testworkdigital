import {
    GET_CATEGORIES_REQUEST,
    GET_CATEGORIES_SUCCESS,
    GET_CATEGORIES_FAIL
} from '../constants/Page'

const initialState = {
  categories: [],
  fetched: false
}

export default function categories(state = initialState, action) {
console.log('reducer categories ', action);
  switch (action.type) {
    case GET_CATEGORIES_REQUEST:
      return { ...state }

    case GET_CATEGORIES_SUCCESS:
      return { 
        ...state, 
        ...action.payload,
        fetched: true }

    case GET_CATEGORIES_FAIL:
      return { ...state }

    default:
      return state;
  }

}
