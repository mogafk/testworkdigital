import axios from 'axios';

import { 
    GET_PRODUCTS_REQUEST,
    GET_PRODUCTS_SUCCESS,
    GET_PRODUCTS_FAIL,
    NEXT_PAGE_REQUEST,
    NEXT_PAGE_SUCCESS,
    NEXT_PAGE_FAIL,
    APPLY_FILTER_START,
    APPLY_FILTER_SUCCESS,
    APPLY_FILTER_FAIL
} from '../constants/Page'

export function fetchProducts(query, projection) {
    return function(dispatch) {
        dispatch({
            type: GET_PRODUCTS_REQUEST
        })
        console.log(query, projection)
        axios.get('/products', {
            params: {
                limit: 9,
                skip: 0
            }
        })
            .then(function(response) {

                dispatch({
                  type: GET_PRODUCTS_SUCCESS,
                  payload: {
                    products: response.data
                  }
                });
            })
            .catch(function(error) {
                console.log('fetchCategories err', error)

                dispatch({
                  type: GET_PRODUCTS_FAIL,
                  error: true,
                  _error: error
                });
            })
    }
}


export function nextPage(query, skip, limit) {
    return function(dispatch) {
        dispatch({
            type: NEXT_PAGE_REQUEST
        })
        axios.get('/products', {
            params: {
                ...query,
                limit: 9,
                skip: (skip+9)
            }
        }).then(function(response) {
                console.log('успех для /products', response);
                dispatch({
                  type: NEXT_PAGE_SUCCESS,
                  payload: {
                    query: query,
                    products: response.data,
                    limit: limit,
                    skip: skip+9
                  }
                });
            })
            .catch(function(error) {
                console.error('fetchCategories err', error)

                dispatch({
                  type: NEXT_PAGE_FAIL,
                  error: true,
                  _error: error
                });
            })
    }
}


export function applyFilter(query) {
    console.log('applyFilter', query);
    return function(dispatch) {
        dispatch({
            type: APPLY_FILTER_START
        })
        axios.get('/products', {params: query})
            .then(function(response) {
                console.log('успех для фильтра /products', response);
                dispatch({
                  type: APPLY_FILTER_SUCCESS,
                  payload: {
                    query,
                    products: response.data
                  }
                });
            })
            .catch(function(error) {
                console.log('fetchCategories err', error)

                dispatch({
                  type: APPLY_FILTER_FAIL,
                  error: true,
                  _error: error
                });
            })
    }

}