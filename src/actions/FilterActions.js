import axios from 'axios';

import { 
    GET_CATEGORIES_REQUEST,
    GET_CATEGORIES_SUCCESS,
    GET_CATEGORIES_FAIL
 } from '../constants/Page'


export function fetchCategories() {
    return function(dispatch) {
        // console.log('fetchCategories run')
        dispatch({
            type: GET_CATEGORIES_REQUEST
        })

        axios.get('/categories')
            .then(function(response) {
                // console.log('fetchCategories response', response);

                dispatch({
                  type: GET_CATEGORIES_SUCCESS,
                  payload: {
                    categories: response.data
                  }
                });
            })
            .catch(function(error) {
                console.log('fetchCategories err', error)

                dispatch({
                  type: GET_CATEGORIES_FAIL,
                  error: true,
                  _error: error
                });
            })

    }
}